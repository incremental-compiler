#include <stdio.h>

#define fixnum_mask 3
#define fixnum_tag 0
#define fixnum_shift 2
#define char_mask 255
#define char_shift 8
#define char_tag 15
#define boolean_mask 127
#define boolean_shift 7
#define boolean_tag 31
#define empty_list 47
#define heap_mask 7
#define pair_tag 1
#define vector_tag 2
#define string_tag 3
#define closure_tag 6

int main(int argc, char** argv) {
  int val = scheme_entry();

  if((val & fixnum_mask) == fixnum_tag) {
    printf("%d\n", val >> fixnum_shift);
  } else if((val & char_mask) == char_tag) {
    printf("%c\n", val >> char_shift);
  } else if((val & boolean_mask) == boolean_tag) {
    if((val >> boolean_shift) == 1) {
      printf("#t\n");
    } else {
      printf("#f\n");
    }
  } else if((val & heap_mask) == pair_tag) {
    printf("pair\n");
  } else if((val & heap_mask) == vector_tag) {
    printf("vector\n");
  } else if((val & heap_mask) == string_tag) {
    printf("string\n");
  } else if((val & heap_mask) == closure_tag) {
    printf("closure\n");
  } else if(val == empty_list) {
    printf("()\n");
  }
  return 0;
}
